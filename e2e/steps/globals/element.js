// const {Given} = require('cucumber');
// const {expect} = require('chai');
import { Given, Then } from "cucumber";
import { expect } from "chai";
import logger from '@wdio/logger'
import Promise from "bluebird";
const log = logger('benchmark')

import adb from "adbkit";
const client = adb.createClient();
const TIMEOUT = 20000;
const SWIPE_DIRECTION = {
  left: "left",
  right: "right",
  up: "up",
  down: "down",
};
let currentTime;
const waitFor = async (el) => {
  // browser.pause(10000)
  let a = await $(el);
  await a.waitForExist(TIMEOUT);
};
Given(/^Click element: "([^"]*)?"$/, (elId) => {
  const el = "~" + elId;
  browser.elementClick($(el).elementId);
});
Given(/^Pause: "([^"]*)?"$/, (time) => {
  browser.pause(time);
});
Given(/^Click element with text: "([^"]*)?"$/, (text) => {
  // ios not supported
  const element = browser.findElement(
    "xpath",
    "//android.widget.TextView[@text='" + text + "']"
  );
  browser.elementClick(element.ELEMENT);
});
Given(/^Wait for element: "([^"]*)?"$/, async (elId) => {
  const el = "~" + elId;
  await waitFor(el);
});

Given(/^Wait for element xpath: "([^"]*)?"$/, async (xpath) => {
  await waitFor(xpath);
});

Given(/^Click element xpath: "([^"]*)?"$/, async (xpath) => {
  const element = browser.findElement(
    "xpath",
    xpath
  );
  browser.elementClick(element.ELEMENT);
});

Given(/^Wait for element with text: "([^"]*)?"$/, (text) => {
  // ios not supported
  waitFor(`//*[@text='${text}']`);
});
Given(/^Element: "([^"]*)?" should have text: "([^"]*)?"$/, (elId, text) => {
  const elementText = browser.getText("~" + elId);
  expect(elementText).to.equal(text);
});

// Given(/^Launch App:$/, async () => {

 
//   const currentTime = new Date().getTime();
//   await browser.startActivity(
//     "com.prudential.pulse.uat",
//     "com.prudential.pulse.MainActivity"
//   );
//   //  await browser.pause(10000)
//   const text = "CONTINUE WITH EMAIL";
//   // const element =  browser.findElement('xpath', "//android.widget.TextView[@text='"+text+"']")
//   const element = await browser.findElement(
//     "xpath",
//     "//android.widget.TextView[@text='CONTINUE WITH EMAIL']"
//   );

//   // await waitFor("~mailLogo");
//   log.warn(element)
//   await waitFor(await $('#' + element.ELEMENT));
//   const endTime = new Date().getTime();
//   const timeDiff = endTime - currentTime;
//   //console.log("timeDiff", timeDiff);
//   log.info("Time to load Home Screen", timeDiff);

//   //browser.pause(TIMEOUT)
// });


Given(/^Launch App:$/, async () => {
  const continueWithEmailXpath =
    "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView";
  const currentTime = new Date().getTime();
  await browser.startActivity(
    "com.prudential.pulse.uat",
    "com.prudential.pulse.MainActivity"
  );
  browser.pause(10000)
  //  await browser.pause(10000)
  //const text = "CONTINUE WITH EMAIL";
  // const element =  browser.findElement('xpath', "//android.widget.TextView[@text='"+text+"']")
/*   const element = await browser.findElement(
    "id",
    "a17f7811-7fb8-4395-bf59-c85b1280edd8"
  );
  const continueWithHomeScreenXpath = "";
  log.info("element continue", element) */
  await waitFor('~loginWithEmail');
  const endTime = new Date().getTime();
  const timeDiff = endTime - currentTime;
  //console.log("timeDiff", timeDiff);
  log.info("Time to load Home Screen", timeDiff);

  //browser.pause(TIMEOUT)
});

// Then('Click on Not Now for FingerPrint Setup', async () => {
//   const notNowButton = 
//   "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView"
//   const element1 = await $(notNowButton)
//   await waitFor(notNowButton)
//   log.warn(element1)
//   // await browser.elementClick(element);
//   const element = await browser.findElement(
//     "xpath",
//     "//android.widget.TextView[@text='Not Now']"
//   );
//   await browser.elementClick(element.ELEMENT);

// });

Then('Click on Not Now for FingerPrint Setup', async () => {
  const notNowButton = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView';
  await waitFor(notNowButton)
  const element = await browser.findElement(
    "xpath",
    "//android.widget.TextView[@text='Not Now']"
  );
  await browser.elementClick(element.ELEMENT);
  // const element = browser.findElement(
  //   "xpath",
  //   notNowButton
  // );
  // browser.elementClick(element.ELEMENT);

});


Then('Calculate Component Throughput', async () => {
  const HomeTab = await browser.findElement(
    "xpath",
    "//android.widget.TextView[@text='Home']"
  );
  const HealthTab = await browser.findElement(
    "xpath",
    "//android.widget.TextView[@text='Health']"
  );
  const FoodTab = await browser.findElement(
    "xpath",
    "//android.widget.TextView[@text='Food']"
  );
  var arr = [];
  var keepCalling = true;
  setTimeout(function () {
      keepCalling = false;
  }, parseInt(process.env["runFor"]));

while (keepCalling) {
      var arr1 = await thoroughPutFunction(HomeTab);
      arr.push(arr1);
      var arr2 = await thoroughPutFunction(HealthTab);
      arr.push(arr2);
      var arr3 = await thoroughPutFunction(FoodTab);
      arr.push(arr3);

}
  // var startTime = Date.now();
  // while ((Date.now() - startTime) < 60000) {
  //     var arr = await thoroughPutFunction(HomeTab);
  //     var arr = await thoroughPutFunction(HealthTab);
  //     var arr = await thoroughPutFunction(FoodTab);
  // }
  // log.info('arr:::',arr)
  let meanTime = average(arr)
  log.info('meanTime:::',meanTime)
  const p25 = quantile(arr, .25);
  log.info('p(25):::',p25)
  const p50 = quantile(arr, .50);
  log.info('p(50):::',p50)
  const p75 = quantile(arr, .75);
  log.info('p(75):::',p75)
  const p95 = quantile(arr, .95);
  log.info('p(95):::',p95)
  const max = quantile(arr, 1.00);
  log.info('max:::',max)
});

const thoroughPutFunction = async (tab) => {
  
    // let arr = [];
    // var endTime = 5;
    // while(endTime != 0){
    
      //  scrollByTextAllForNow();
      let timeToLoad = await scrollByTextAllForNow(tab);
      // let timeToLoadHealth = await scrollByTextAllForNow(HealthTab);
      // let timeToLoadFood = await scrollByTextAllForNow(FoodTab);
      // a.push(timeToLoad)
      // arr.push(timeToLoadHealth)
      // arr.push(timeToLoadHealth)
      log.info('timeToLoad:::',timeToLoad);
      // log.info('timeToLoadHealth:::',timeToLoadHealth);
      // log.info('timeToLoadFood:::',timeToLoadFood);

      return timeToLoad
      // endTime --;

    // }
   
  };
  
  const average = (num) => {
      return num.reduce((a, b) => (a + b)) / num.length;
  }

  const asc = arr => arr.sort((a, b) => a - b);
  const quantile = (arr, q) => {
    const sorted = asc(arr);
    const pos = (sorted.length - 1) * q;
    const base = Math.floor(pos);
    const rest = pos - base;
    if (sorted[base + 1] !== undefined) {
        return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
    } else {
        return sorted[base];
    }
};

  const scrollByTextAllForNow = async(tab) =>{
    const currentTime = new Date().getTime();
    await browser.elementClick(tab.ELEMENT);
    const notNowButton ='/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]';
    await waitFor(notNowButton);
  const endTime = new Date().getTime();
  const timeDiff = endTime - currentTime;
  return timeDiff;
  // console.log("components load time", timeDiff);
  }

Then(/^MemoryUseage:$/, async () => {

  //const serial = "emulator-5554"
  client.listDevices()
  .then(function(devices) {
    return Promise.map(devices, function(device) {
      return client.shell(device.id, 'dumpsys meminfo com.prudential.pulse.uat')
        // Use the readAll() utility to read all the content without
        // having to deal with the events. `output` will be a Buffer
        // containing all the output.
        .then(adb.util.readAll)
        .then(function(output) {
          //console.log('[%s] %s', device.id, output.toString().trim())
          log.info("RAM Usage:" ,output.toString().trim().split("TOTAL")[1].trim().split(" ")[0].trim())
        })
    })
  })
  .then(function() {
    console.log('Done.')
  })
  .catch(function(err) {
    console.error('Something went wrong:', err.stack)
  })

  //browser.pause(TIMEOUT)
});

Then(/^CpuUseage:$/, async () => {

  //const serial = "emulator-5554"
  client.listDevices()
  .then(function(devices) {
    return Promise.map(devices, function(device) {
      return client.shell(device.id, 'dumpsys cpuinfo com.prudential.pulse.uat')
        // Use the readAll() utility to read all the content without
        // having to deal with the events. `output` will be a Buffer
        // containing all the output.
        .then(adb.util.readAll)
        .then(function(output) {
          //console.log('[%s] %s', device.id, output.toString().trim())
          const line = output.toString().split(/(?:\r\n|\r|\n)/g)
          for(let i=0; i<line.length; i++){
            //console.log("line["+i+"]",line[i])
            if(line[i].includes("com.prudential.pulse.uat")){
              log.info("CPU Usage:" ,line[i].trim().split("/com.prudential.pulse.uat")[0].trim().split(" ")[0])
            }
          }          
        })
    })
  })
  .then(function() {
    console.log('Done.')
  })
  .catch(function(err) {
    console.error('Something went wrong:', err.stack)
  })

  //browser.pause(TIMEOUT)
});

Given(/^Scroll by text: "([^"]*)?"$/, (text) => {
  //const args = data.split(",");
  //const el = '~' + args[2];
  //browser.touchScroll(parseInt(args[0]),parseInt(args[1]))
  const currentTime = new Date().getTime();
  const bottomElementSelector = `new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text("${text}"))`;
  const bottomEl = $(`android=${bottomElementSelector}`);
  const endTime = new Date().getTime();
  const timeDiff = endTime - currentTime;
  console.log("components load time", timeDiff);
});
Given(/^Swipe "?([^"]*)"? element: "([^"]*)?"$/, (direction, elId) => {
  const selector = `~${elId}`;
  const time = 600;
  switch (direction) {
    case SWIPE_DIRECTION.right:
      browser.swipeRight(selector, time);
      break;
    case SWIPE_DIRECTION.left:
      browser.swipeLeft(selector, time);
      break;
    case SWIPE_DIRECTION.down:
      browser.swipeDown(selector, time);
      break;
    default:
      browser.swipeUp(selector, time);
  }
});
