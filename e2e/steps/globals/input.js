// const axios = require('axios');
// const cheerio = require('cheerio');
// const { Given, Then } = require('cucumber');
// const { otpEmail } = require("../helpers/email");
// const adb = require("adbkit");
import axios from "axios";
import cheerio from "cheerio";
import { Given, Then } from "cucumber";
import { otpEmail } from "../helpers/email";
import adb from "adbkit";
const client = adb.createClient();
// import { isAndroid } from './utils'
let email = null;
let hash = null;
let otp = null;
export const setValueSafe = async (selector, text) => {
  (await $(selector)).sendKeys(text);
  // if ios has issues with typing too fast on emulators - use this and isAndroid on above
  // for (let i = 0; i < text.length; i++) {
  //   browser.element(selector).setValue(text[i])
  // }
};

export const generateMail = async () => {
  try {
    const response = await axios({
      url: "https://api.mytemp.email/1/inbox/create?sid=73358&task=27&tt=3139",
      method: "post",
    });
    email = response.data.inbox;
    hash = response.data.hash;
  } catch (e) {
    console.log("generate email failed ", e.message);
  }
};

export const getEmail = async () => {
  try {
    const r = await axios({
      url: `https://api.mytemp.email/1/inbox/check?inbox=${email}&hash=${hash}&sid=8635154&task=3&tt=5`,
    });
    const response = r.data;
    const emails = response["emls"];
    const filterEmail = emails.filter((email) => {
      return (
        email.from_address === otpEmail.emailFrom &&
        email.status === otpEmail.status &&
        email.from_name === otpEmail.emailFromName &&
        email.subject === otpEmail.subject
      );
    });
    const getUnreadEmail = await axios({
      url: `https://api.mytemp.email/1/eml/get?eml=${filterEmail[0].eml}&hash=${filterEmail[0].hash}`,
    });
    const $ = cheerio.load(getUnreadEmail.data.body_html);
    otp = $(".mytemp-e22").text();
  } catch (e) {
    console.log(" getEmail error", e.message);
  }
};
Given(/^Type "([^"]*)?" into "([^"]*)?"$/, (text, id) => {
  const selector = "~" + id;
  //setValueSafe(selector, text);
  //const element = browser.findElement('xpath', "//android.widget.EditText[@text='"+id+"']")
  //console.log("***selector***",$(selector))
  browser.$(selector).elementSendKeys($(selector).elementId, text);
});

Given(/^xpath type "([^"]*)?" into "([^"]*)?"$/, (text, xpath) => {
  const element = browser.findElement("xpath", xpath);
  //setValueSafe(selector, text);
  //const element = browser.findElement('xpath', "//android.widget.EditText[@text='"+id+"']")
  browser.elementSendKeys(element.ELEMENT, text);
});

Given(/^Tap off keyboard$/, () => {
  browser.hideDeviceKeyboard("tapOutside");
});
Given(/^Press submit$/, () => {
  browser.hideDeviceKeyboard("pressKey", "Done");
});

Given(/^GenerateEmail$/, async () => {
  await generateMail();
});

Given(/^FillEmail: "([^"]*)?"$/, async (id) => {
  const selector = "~" + id;
  browser.$(selector).elementSendKeys($(selector).elementId, email);
});

Then(/^GetCpuUseage$/, async () => {
  const data = client.shell("dumpsys cpuinfo");
  console.log("data", data);
});

Given(/^ReadOtp$/, async () => {
  await getEmail();
});
Given(/^EnterOtp:$/, () => {
  const element = browser.findElement(
    "xpath",
    "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText"
  );
  browser.elementSendKeys(element.ELEMENT, otp);
});
