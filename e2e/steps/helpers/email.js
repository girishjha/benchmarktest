export const otpEmail = {
    "emailFrom" : "no_reply@prudential.com.sg",
    "emailFromName": "NoReply",
    "subject": "Pulse - Account creation request",
    "status": "unread"
}