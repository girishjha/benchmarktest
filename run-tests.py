#from invoke import task
import logging
import time
import boto3
import requests
import sys
import os

REGION = 'us-west-2'
PROJECT_NAME = 'Pulse_Testing'  # for iOS device use  the project = PulseAutomation-iOS
DEVICE_POOL_NAME = 'device_pool1'  # for iOS device use  the pool = iOSPool
RUN_TIMEOUT_SECONDS = 60 * 20
WEB_URL_TEMPLATE = 'https://us-west-2.console.aws.amazon.com/devicefarm/home?#/projects/%s/runs/%s'
APP_TYPE = 'ANDROID_APP' # for iOS device use the value  IOS_APP
TEST_TYPE = 'APPIUM_NODE_TEST_PACKAGE'
# PROXY= { "http": "http://127.0.0.1:8888", "https": "http://127.0.0.1:8888", "ftp": "http://127.0.0.1:8888" }
PROXY= { "http": None, "https":None, "ftp": None }
VERIFY_SSL = True if PROXY["https"] is None else False
SPEC_TYPE='APPIUM_NODE_TEST_SPEC'
TEST_RUN_TYPE='APPIUM_NODE'
TEST_ZIP_PATH='./integration-tests.zip'
TEST_SPEC_PATH='./test_config.yml'
APP_PATH='../one-pulse-app/android/app/build/outputs/apk/uat/release/app-uat-arm64-v8a-release.apk'
#os.environ["NLS_LANG"] = "AMERICAN_AMERICA.AL32UTF8"

device_farm = boto3.client('devicefarm', region_name=REGION)
s3 = boto3.client('s3', region_name=REGION)
logger = logging.getLogger(__name__)

#task
def hi(c, name):
    print("Hi {}!".format(name))

#@task
def build(c):
    print("Building!")

#@task(optional=['app_type', 'device_pool', 'project_name'])
def trigger_devicefarm_test(app_path, test_zip_path, spec_path, app_type=None, device_pool=None, project_name = None):
    """
    Triggers the test in AWS device farm
    """
    # print(app_path)
    logger = logging.getLogger('trigger_test')
    app_name = app_path.split('/')[-1]
    test_zip_name=test_zip_path.split('/')[-1]
    spec_file_name = spec_path.split('/')[-1]
    device_pool_name = device_pool if device_pool is not None else DEVICE_POOL_NAME
    app_type = app_type if app_type is not None else APP_TYPE
    project_name = project_name if project_name is not None else PROJECT_NAME
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)
    logger.info('app path ' + app_path)
    logger.info('Fetching project arn...')
    project_arn = get_project_arn(project_name)
    logger.info('Project ARN: %r' % project_arn)

    logger.info("fetching device pool arn...")
    device_pool_arn = get_device_pool(project_arn, device_pool_name)
    logger.info('Device pool: %r' % device_pool_arn)
   
    logger.info('uploading app...')
    app_arn = create_upload(project_arn,app_type,app_name,app_path)
    wait_for_upload(app_arn)
    logger.info('App: %s' % app_arn)

    logger.info('uploading test package...')
    test_package_arn = create_upload(project_arn,TEST_TYPE,test_zip_name, test_zip_path)
    wait_for_upload(test_package_arn)
    logger.info('Test package: %s' % test_package_arn)    
    
    logger.info('uploading test spec...')
    test_spec_arn = create_upload(project_arn,SPEC_TYPE,spec_file_name, spec_path)
    wait_for_upload(test_spec_arn)
    logger.info('Spec ARN: %s' % test_spec_arn)

    logger.info('Scheduling test run...')
    test_run_arn = schedule_run(project_arn,name='Pulse Automation',device_pool_arn=device_pool_arn,app_arn=app_arn,test_package_arn=test_package_arn, test_spec_arn=test_spec_arn)
    logger.info('Scheduled test run %r' % test_run_arn)
    logger.info('View scheduled run at %s' % get_run_web_url(project_arn, test_run_arn))
    success = wait_for_run(test_run_arn)
    #logger.info("Test Succeeded:  {0}".format(success))
    response = get_artifacts(test_run_arn)
    #logger.info(response)
    logger.info('Success :{0}'.format(success))
    return 0 if success else -1


def get_artifacts(arn):
    response = device_farm.list_artifacts(type='FILE', arn=arn)
    for artifact in response["artifacts"]:
        if artifact["type"] == "TESTSPEC_OUTPUT":
            test_spec_url = artifact["url"]
            logger.info(test_spec_url)
            test_spec_response = requests.get(test_spec_url)
            status_code = test_spec_response.status_code
            #logger.info("Test Spec Output: {0}".format(status_code))
            if status_code == 200:
                response_data = test_spec_response.text
                #logger.info("Test Spec Log: \n {0}".format(response_data))
                run_arn = arn.split(':')[-1].replace('/', '_')
                path = "logs/test-spec-output-" + run_arn + ".txt"
                logger.info("New file Path: {0}".format(path))
                new_file = open(path, "w")
                new_file.write(response_data)
                new_file.close()

    return response


def get_project_arn(name):
    for project in device_farm.list_projects()['projects']:
        if project['name'] == name:
            return project['arn']
    raise KeyError('Could not find project %r' % name)

def get_device_pool(project_arn, name):
    for device_pool in device_farm.list_device_pools(arn=project_arn)['devicePools']:
        logger.info(device_pool['name'])
        if device_pool['name'] == name:
            return device_pool['arn']
    raise KeyError('Could not find device pool %r' % name)

def _upload_presigned_url(url, file_path):
    with open(file_path, "rb") as fp:
        data = fp.read()
        result = requests.put(url, data=data, headers={'content-type': 'application/octet-stream'}, proxies=PROXY, verify=VERIFY_SSL)
        assert result.status_code == 200

def create_upload(project_arn, upload_type, name, file_path):
    # name needs to be a file name like app-releaseProduction.apk, not "Android App"
    logger.info('Uploading %s %r' % (upload_type, file_path))
    result = device_farm.create_upload(
        projectArn=project_arn,
        name=name,
        type=upload_type,
        contentType='application/octet-stream',
    )
    upload = result['upload']
    _upload_presigned_url(upload['url'], file_path)
    return upload['arn']

def schedule_run(project_arn, name, device_pool_arn, app_arn, test_package_arn, test_spec_arn):
    logger.info('Scheduling test run %r' % name)
    logger.level = logging.DEBUG
    result = device_farm.schedule_run(
        projectArn=project_arn,
        appArn=app_arn,
        devicePoolArn=device_pool_arn,
        name=name,
        test={
            'type': TEST_RUN_TYPE,
            'testPackageArn': test_package_arn,
            'testSpecArn': test_spec_arn,
        }
    )
    run = result['run']
    return run['arn']

def _poll_until(method, arn, get_status_callable, success_statuses, timeout_seconds=10):
    check_every_seconds = 10 if timeout_seconds == RUN_TIMEOUT_SECONDS else 1
    start = time.time()
    while True:
        result = method(arn=arn)
        current_status = get_status_callable(result)
        if current_status in success_statuses:
            return result
        logger.info('Waiting for %r status %r to be in %r' % (arn, current_status, success_statuses))
        now = time.time()
        if now - start > timeout_seconds:
            raise StopIteration('Time out waiting for %r to be done' % arn)
        time.sleep(check_every_seconds)

def wait_for_upload(arn):
    return _poll_until(
        device_farm.get_upload,
        arn,
        get_status_callable=lambda x: x['upload']['status'],
        success_statuses=('SUCCEEDED', ),
    )

def wait_for_run(test_package_arn):
    result = _poll_until(
        device_farm.get_run,
        test_package_arn,
        get_status_callable=lambda x: x['run']['status'],
        success_statuses=('COMPLETED', ),
        timeout_seconds=RUN_TIMEOUT_SECONDS,
    )

    final_run = result['run']
    logger.info('Final run counts: %(counters)s' % final_run)
    return final_run['result'] == 'PASSED'

def get_run_web_url(project_arn, test_run_arn):
    # project_arn = arn:aws:devicefarm:us-west-2:foo:project:NEW-ARN-HERE
    # test_run_arn = arn:aws:devicefarm:us-west-2:foo:run:project-arn/NEW-ARN-HERE
    project_arn_id = project_arn.split(':')[6]
    test_run_arid = test_run_arn.split('/')[1]
    return WEB_URL_TEMPLATE % (
        project_arn_id,
        test_run_arid,
    )


def install_npm_packages():
    result = os.system('npm install')
    if result != 0:
        sys.exit(result)

    result = os.system('npm run package')
    if result != 0:
        sys.exit(result)



if __name__ == '__main__':
    app_path = sys.argv[1]
    app_type = sys.argv[2]
    device_pool = sys.argv[3]
    project_name = sys.argv[4]
    install_npm_packages()

    #APP_PATH = app_path if app_path is not None else APP_PATH
    #APP_TYPE = app_type if app_type is not None else APP_TYPE
    #DEVICE_POOL_NAME = device_pool if device_pool is not None else DEVICE_POOL_NAME
    #PROJECT_NAME = project_name if project_name is not None else PROJECT_NAME

    APP_PATH = app_path
    APP_TYPE = app_type
    DEVICE_POOL_NAME = device_pool
    PROJECT_NAME = project_name

    status = trigger_devicefarm_test(app_path=app_path, test_zip_path=TEST_ZIP_PATH, spec_path=TEST_SPEC_PATH,
                                     app_type=app_type, device_pool=device_pool, project_name=project_name)
    sys.exit(status)
